import numpy as np
import pandas as pd

data = pd.read_csv('data.csv')
data.set_index(['Componente', 'Articulo'])

print("""\
+-----------------------------------------------------------------------+
|--------------------Programa de Perfiles de Recurso--------------------|
+-----------------------------------------------------------------------+\
""")
print("\nDatos:")
print(data)

np_data = np.zeros((data.CentroTrabajo.unique().size, data.Periodo.max())) 
df = pd.DataFrame(data=np_data, columns = data.Periodo.unique(), index = data.sort_values('CentroTrabajo').CentroTrabajo.unique())

Tablas = np.array([])

for a in data.Articulo.unique():
    dataArt = data.loc[data['Articulo'] == a]
    for ct in dataArt.sort_values('CentroTrabajo').CentroTrabajo.unique():
        periodos = dataArt.loc[data['CentroTrabajo'] == ct]['Periodo']
        for p in periodos:
            to = np.sum(dataArt.loc[(dataArt['CentroTrabajo']==ct) & (dataArt['Periodo'] ==p)]['TiempoOperacion'])
            df[p][ct] = to
    if a == "A":
        TablaA = df.copy()
    else:
        TablaB = df.copy()
    df.loc[:,:] = 0

print("""\
\n\n+-----------------------------------------------------------------------+
|-------------------Horas estándar para cada operación------------------|
+-----------------------------------------------------------------------+\
""")
print("\nHoras estándar para el producto A")
print(TablaA)
print("Horas estándar para el producto B")
print(TablaB)

MPS = pd.read_csv('MPS.csv')
MPS = MPS.set_index("Productos-Periodos")

print("""\
\n+-----------------------------------------------------------------------+
|----------------------Plan Maestro de Producción-----------------------|
+-----------------------------------------------------------------------+\n\
""")
print(MPS)

columnas = np.array(['CT', 'Deuda', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', 'HrTotales', '% por CT'])
Final = pd.DataFrame(data = np.zeros((3, 17)), columns = columnas)
Final['CT'] = data.sort_values('CentroTrabajo').CentroTrabajo.unique()
Final = Final.set_index('CT')

print("""\
\n+-----------------------------------------------------------------------+
|-------------------Cantidades MPS para cada periodo--------------------|
+-----------------------------------------------------------------------+\
""")

for p in MPS.columns[:-1]:
    print("\nTiempo requerido durante el periodo " + p)
    TAenP = TablaA*MPS[p]['A']
    TBenP = TablaB*MPS[p]['B']
    if (int(p)-1) == 0:
        TAenP.columns = [int(p)-3, int(p)-2, p]
        TBenP.columns = [int(p)-3, int(p)-2, p]
        for ct in range(len(Final.index)):
            Final.iloc[ct, 0] = sum(TAenP.iloc[ct, 0:2] + TBenP.iloc[ct, 0:2])
            Final.iloc[ct, 1] = Final.iloc[ct, 1] + TAenP.iloc[ct, 2] + TBenP.iloc[ct, 2]
        
    elif (int(p)-2) == 0:
        TAenP.columns = [int(p)-3, int(p)-1, p]
        TBenP.columns = [int(p)-3, int(p)-1, p]
        for ct in range(len(Final.index)):
            Final.iloc[ct, 0] = Final.iloc[ct, 0] + TAenP.iloc[ct, 0] + TBenP.iloc[ct, 0]
            Final.iloc[ct, 1] = Final.iloc[ct, 1] + TAenP.iloc[ct, 1] + TBenP.iloc[ct, 1]
            Final.iloc[ct, 2] = Final.iloc[ct, 2] + TAenP.iloc[ct, 2] + TBenP.iloc[ct, 2]
            
    else:
        TAenP.columns = [int(p)-2, int(p)-1, p]
        TBenP.columns = [int(p)-2, int(p)-1, p]
        for ct in range(len(Final.index)):
            Final.iloc[ct, int(p)] = Final.iloc[ct, int(p)] + TAenP.iloc[ct, 2] + TBenP.iloc[ct, 2]
            Final.iloc[ct, int(p)-1] = Final.iloc[ct, int(p)-1] + TAenP.iloc[ct, 1] + TBenP.iloc[ct, 1]
            Final.iloc[ct, int(p)-2] = Final.iloc[ct, int(p)-2] + TAenP.iloc[ct, 0] + TBenP.iloc[ct, 0]
    print("\nProducto A\n", TAenP)
    print("\nProducto B\n", TBenP)

Final['HrTotales'] = Final.sum(axis = 1)
Final.loc['Total'] = Final.sum(axis = 0)
Final['% por CT'] = Final['HrTotales'] / Final.iloc[-1, -2] * 100

print("""\
\n\n+-----------------------------------------------------------------------+
|---------------Información de capacidad en fase de tiempo--------------|
+-----------------------------------------------------------------------+\n\
""")
print(Final, "\n\n")
